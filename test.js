const { expect, assert } = require('chai');
const puppeteer = require('puppeteer');
const exampleTester = require('./index');

describe('Puppeteer', () => {
  before(async function () {
    this.browser = await puppeteer.launch({
      headless: false,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
      ],
    });
    this.page = await this.browser.newPage();
    this.tester = exampleTester(this.page);
  });

  after(async function () {
    await this.browser.close();
    process.exit(0);
  });

  describe('Startup', () => {
    it('should start', async function () {
      assert.equal('object', typeof this.browser);
    });
  });

  describe('In Browser', () => {
    describe('Empty Tab', () => {
      it('url should be blank', async function () {
        const url = await this.tester.getLocation();
        expect(url).to.include('about:blank');
      });
    });
    function testWebsite(url) {
      describe(url, async () => {
        it(`navigate: should navigate ${url}`, async function () {
          const ok = await this.tester.navigate(url);
          expect(ok).to.true;
        });
        it(`getLocation: url should have ${url}`, async function () {
          const targetUrl = await this.tester.getLocation(this.page);
          expect(targetUrl).to.include(url);
        });
      });
    }
    testWebsite('https://www.example.com');
    testWebsite('https://www.expedia.com');
  });
});
